import 'package:flutter/material.dart';
import '../screens/home.dart';
import '../screens/testPage.dart';

class NavigationService {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => Home());
      case '/test':
        return MaterialPageRoute(builder: (_) => TestPage());
      default:
    }
  }

  static final GlobalKey<NavigatorState> navigatorKey =
      GlobalKey<NavigatorState>();

  static Future<dynamic> navigateTo(String routeName) {
    return navigatorKey.currentState.pushNamed(routeName);
  }

  static goBack() {
    return navigatorKey.currentState.pop();
  }
}

// Route route = ModalRoute.of(context);
//     final routeName = route?.settings?.name;
//     if (routeName != null && routeName != nav) {
//       Navigator.of(context).pushNamed(nav);
//       print(route.settings.name);
//     }