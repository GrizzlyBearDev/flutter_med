import 'package:flutter/material.dart';
import '../widgets/customAppBar.dart';

class TestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // final routeArgs =
    //     ModalRoute.of(context).settings.arguments as Map<String, String>;
    // final id = routeArgs['id'];

    return Container(
      child: ElevatedButton(
        onPressed: () {
          Navigator.of(context).pushNamed(
            '/',
          );
        },
        child: Text('/'),
      ),
    );
  }
}
