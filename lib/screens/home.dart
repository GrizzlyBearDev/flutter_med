import 'package:flutter/material.dart';
import '../widgets/customAppBar.dart';

class Home extends StatefulWidget {
  final String title = 'Home';

  @override
  _Home createState() => _Home();
}

class _Home extends State<Home> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).pushNamed(
                  '/test',
                  // arguments: {'id': '112312'},
                );
              },
              child: Text('to test'),
            ),
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          print('he');
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}
