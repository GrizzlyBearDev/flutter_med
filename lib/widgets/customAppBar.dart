import 'package:flutter/material.dart';
import '../services/navigationService.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  @override
  final Size preferredSize;

  final String title;

  CustomAppBar(this.title) : preferredSize = Size.fromHeight(50.0);

  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.blue,
        // image: DecorationImage(
        //   image: NetworkImage(
        //       'https://images.unsplash.com/photo-1615370183912-4e68cb0ac846?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80'),
        //   fit: BoxFit.cover,
        // ),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(1),
            spreadRadius: 1,
            blurRadius: 4,
            offset: Offset(0.5, 0.5), // changes position of shadow
          ),
        ],
      ),
      padding: EdgeInsets.all(20),
      width: 250,
      child: ListView.builder(
        padding: const EdgeInsets.all(0),
        itemCount: entries.length,
        itemBuilder: (BuildContext context, int index) {
          return NavItem(
            text: entries[index],
            to: '/',
          );
        },
      ),
    );
  }
}

class NavItem extends StatelessWidget {
  String text;
  String to;

  NavItem({
    this.text,
    this.to,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
      ),
      margin: EdgeInsets.only(bottom: 10),
      height: 50,
      child: Ink(
        child: InkWell(
          highlightColor: Colors.red,
          splashColor: Colors.redAccent,
          onTap: () {
            NavigationService.navigateTo(to);
          },
          child: Center(
            child: Text(text),
          ),
        ),
      ),
    );
  }
}
