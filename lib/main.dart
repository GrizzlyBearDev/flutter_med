import 'package:flutter/material.dart';
import 'package:desktop_window/desktop_window.dart';
import 'package:flutter/rendering.dart';
import 'package:med/screens/home.dart';
import 'package:med/screens/testPage.dart';
import 'package:get_it/get_it.dart';
import './layout.dart';

void main() async {
  runApp(MyApp());
  // debugPaintSizeEnabled = true; //
  await DesktopWindow.setMinWindowSize(Size(1200, 722));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Layout(),
    );
  }
}
