import 'package:flutter/material.dart';
import './widgets/customAppBar.dart';
import './services/navigationService.dart';

class Layout extends StatefulWidget {
  final String title = 'Home';

  @override
  _Layout createState() => _Layout();
}

class _Layout extends State<Layout> {
  void _selectScreen() {
    setState(() {
      // currentTab = tabItem;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Row(
      children: [
        CustomAppBar('Hello'),
        Expanded(
          child: Container(
            padding: EdgeInsets.only(top: 20, left: 20, right: 20),
            child: Navigator(
              key: NavigationService.navigatorKey,
              onGenerateRoute: NavigationService.generateRoute,
              initialRoute: '/',
            ),
          ),
        ),
      ],
    ));
  }
}
